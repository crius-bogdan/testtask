(function(doc) {

    const menu = doc.querySelectorAll('.menu'),
          header = doc.querySelector('.header');

    doc.addEventListener('click', function(e) {
        let target = e.target;

        while(target != this) {
            if (target.classList.contains('mob-btn')) {
                target.classList.toggle('active');
                for(let i = 0; i < menu.length; i++) {
                    menu[i].classList.toggle('open');
                }
            }


            target = target.parentNode;
        }

    });
    doc.addEventListener('scroll', function(e){
        let rect = doc.querySelector('.text-section').getBoundingClientRect();
        console.log(rect.y);

        if (rect.y < 20) {
            doc.querySelector('.navigation').classList.add('stiky')
        } else {
            doc.querySelector('.navigation').classList.remove('stiky')
        }
    });


})(document);